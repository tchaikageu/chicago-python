# Règles du jeu

Le *Chicago* est un jeu qui mixe le poker et la belote.
Il se joue de deux à quatres joueurs avec un jeu de 32 cartes. Chaque joueur joue pour lui.

## Ordre des cartes

Par ordre croissant de rang

7, 8, 9, 10, V, D, R, As

## Les combinaisons

|Nom|Description|Points|
|:-----|:-----|:-----|
|Paire|Deux cartes identiques (par ex. 2 rois)|1 point|
|Double paire|Deux paires (une paire de dame et une paire de 9)|2 points|
|Brelan|Trois cartes identiques (par ex. 3 cartes de valeur 7)|3 points|
|Suite|5 cartes qui se suivent. Par ex. 7 de coeur, 8 de pique, 9 de pique, 10 de carreau, V de trèfle|4 points|
|Full|une paire accompagnée d'un brelan|5 points|
|Couleur|5 cartes de la même couleur. Par ex. 5 cartes de couleur coeur|6 points|
|Carré|4 cartes identiques (sauf des valets) |7 points|
|Quinte flush|Suite à la couleur. Par ex. 8, 9, 10, V, D à pique|8 points|
|Carré de valets|Comme son nom l'indique : 4 valets ;)|9 points, bat toutes les autres combinaisons et remet les autres joueurs à un score de 0.|

## Ordre des couleurs

Dans l'ordre décroissant :
Pique, Coeur, Carreau, Trèfle

♠ 	♥ 	♦ 	♣

Sert à départager les joueurs en cas de combinaison égale. Si deux joueurs ont une paire de valets, c'est celui qui a en main le valet de pique qui remporte le point.

## La grille des scores

| El Profesor (38)      | Rio (23)              | Tokyo (5)   | Nairobi (31)          | s   |
|-----------------------|-----------------------|-------------|-----------------------|-----|
| \|\|\|\|\| \|\|\|\|\| | \|\|\|\|\| \|\|\|\|\| | \|\|\|\|\|  | \|\|\|\|\| \|\|\|\|\| |     |
| \|\|\|\|\| \|\|\|\|\| | \|\|\|\|\| \|\|\|\|\| |             | \|\|\|\|\| \|\|\|\|\| |     |
| \/\/\/\/\/ \/\/\/\/\/ | \/\/\/                |             | \/\/\/\/\/ \/\/\/\/\/ | *   |
| \|\|\|\|\| \|\|\|     |                       |             | \|                    |     |
|                       |                       |             |                       | *   |
|                       |                       |             |                       |     |
|                       |                       |             |                       |     |


Chaque point marqué est noté par une barre verticale (oblique pour les points situés dans la zone de merde). Chaque ligne représente 10 points. Une fois les 10 points inscrits sur la ligne on passe à la suivante.
Ce jusqu'à ce qu'un des joueurs complète sa colonne.

Le *  indique la *merde*. Le joueur dont les points se trouvent sur cette ligne ne peut pas changer de carte.
Si un joueur est dans la *merde*, les autres ne peuvent pas annoncer de *Chicago*

Sur la grille ci-dessus, les joueurs qui ont entre 21 et 30 points compris (ici Rio) et entre 41 et 50 points inclus sont dans la *merde*.

# Déroulement de la partie

Chaque tour se joue en 2 phases.

## 1ère phase

Le donneur distribue 5 cartes une par une à tous les joueurs.
Dans cette phase, les joueurs ont la possibilité de changer de 0 à 5 cartes, le but étant d'obtenir la plus grosse combinaison (voir tableau).

Si un joueur ne veut changer qu'une carte, le donneur retourne la première carte de la pile et la montre à tout le monde. Le joueur a la possibilité de prendre ou refuser cette carte. S'il la refuse, la carte refusée est mise au dessous de la pile. Le donneur distribue la carte suivante au joueur face cachée.

Une fois que tout le monde changé ses cartes, les joueurs annoncent à tour de rôle la combinaison obtenue. Si un joueur précédent a déjà annoncé une combinaison plus forte que la sienne, le joueur dit simplement "Pas mieux" afin de ne pas dévoiler son jeu. S'il n'a rien, le joueur dit "Rien" ou si une combinaison a déjà été annoncée, il peut dire "Pas mieux"

Le joueur qui a obtenu la combinaison la plus forte (on compare les combinaisons, ensuite pour une même combinaison, la valeur des cartes et la couleur la plus forte en cas d'égalité) marque le nombre de points correspondant à sa combinaison.

Ainsi une paire de dames est plus forte qu'une paire de 8. Si des joueurs ont une combinaison identique à la même valeur de cartes, celui qui a la couleur la plus forte remporte les points. Par exemple, si 2 joueurs ont une paire de 10. Celui qui a le 10 de pique emporte le point.

Pour une suite par exemple, on regarde d'abord la valeur de la carte la plus haute de la suite, puis en cas d'égalité on regarde celui qui a la couleur la plus forte pour cette carte.

Les joueurs peuvent à nouveau changer le nombre de cartes désiré. Le but étant de faire la dernière plie de la seconde phase pour gagner 5 points (ou 10 s’il a annoncé un *Chicago*) et/ou avoir la meilleure combinaison dans la phase suivante.

## 2ème phase:

Avant de commencer à poser les cartes, un ou plusieurs joueur peu(ven)t annoncer un *Chicago* s’il(s) est/sont sûr(s) de remporter la dernière plie (voir détail plus bas).
.
Le premier joueur du tour, c'est à dire celui juste après le donneur, doit poser une carte devant lui face apparente. Cette carte est considérée comme un *atout*. Les autres joueurs sont obligés de fournir à la couleur s’ils le peuvent sinon ils se défaussent d’une carte d’une autre couleur qui sera forcément perdante.
joueur
Le suivant s'il en possède une de la même couleur doit la poser sinon il se défausse d'une carte d'une autre couleur toujours devant lui. Et ainsi de suite. Une fois que tous les joueurs ont déposé une carte devant eux, celui qui a la carte de rang le plus fort à la couleur demandée par le premier joueur gagne. Les cartes de couleur différente
à celle demandée sont forcément perdantes. C'est au tour du joueur qui a remporté la plie de commencer à jouer. Il pose la carte de son choix sur celle qu'il a déjà devant lui. On recommence de la même manière jusqu'à la dernière carte de la main. Le joueur qui remporte la dernière plie (c'est à dire la cinquième) empoche 5 points ou 10 s'il a annoncé un *Chicago*.

On compare à nouveau les combinaisons de chacun des joueurs. Celui qui a la plus forte remporte le nombre correspondant à sa combinaison. Ceci de la même manière qu'en phase 1.

Le cartes jouées sont remise à la fin de la pioche, le joueur qui vient de distribuer les cartes coupe pour le donneur suivant et on recommence en phase 1.

La partie se termine lorsque un des joueurs a fini de remplir sa colonne. Dans le cas de cette grille présentée plus haut, lorsqu'un joueur dépasse les 70 points.


### Le Chicago

Au début de la 2nde phase, un ou plusieurs joueurs peu(ven)t annoncer *Chicago* s'il est sûr de remporter la dernière plie. L'annonce doit être faite avant que la première carte ne soit posée. S'il réussi le contrat, il remporte un bonus de 5 points supplémentaires et donc obtiendra 10 points à la fin de la deuxième phase. Par contre, s'il échoue il perdra 5 points sur son score actuel.

### La merde

Comme indiqué plus haut :
- Quand il y a un joueur dans la *merde* personne ne peut annoncer de *Chicago*.
- Le(s) joueur(s) dans la *merde* ne peu(ven)t pas chager de cartes.

