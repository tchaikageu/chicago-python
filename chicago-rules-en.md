# Presenting

Chicago is a game that mixes poker and belote. It is played by two to four players with a deck of 32 cards. Each player plays for himself

## Card ranking

In ascending order of rank:

7, 8, 9, 10, J, Q, K, A

## Combinations

|Name|Description|Points|
|:-----|:-----|:-----|
|One pair|Two identical cards (eg 2 kings)|1 point|
|Two pair|Two pairs (a pair of Queen and a pair of 9)|2 points|
|Three of a kind|Three identical cards (eg 3 value cards 7)|3 points|
|Straight|5 consecutive cards. For ex. 7 of heart, 8 of spade, 9 of spade, 10 of diamond, Jack of club|4 points|
|Full house|One pair and tree of a kind|5 points|
|Flush|5 cards of the same suit. Eg. 5 heart suit cards|6 points|
|Four of a kind|4 identical cards (except Jacks) |7 points|
|Straight flush|Straight with same suit. Eg. 8, 9, 10, J, Q of spade|8 points|
|Four Jacks|As its name suggests: 4 Jacks ;)|9 points, beats all other combinations and resets the other players to a score of 0.|

## Suit order

In descending order:
Spade, Heart, Diamond, Club

♠ 	♥ 	♦ 	♣
Used to decide between players in the event of an equal combination. If two players have a pair of Jacks, the player with the Jack of spades wins the point.

## The scores grid

| El Profesor (38)      | Rio (23)              | Tokyo (5)   | Nairobi (31)          | s   |
|-----------------------|-----------------------|-------------|-----------------------|-----|
| \|\|\|\|\| \|\|\|\|\| | \|\|\|\|\| \|\|\|\|\| | \|\|\|\|\|  | \|\|\|\|\| \|\|\|\|\| |     |
| \|\|\|\|\| \|\|\|\|\| | \|\|\|\|\| \|\|\|\|\| |             | \|\|\|\|\| \|\|\|\|\| |     |
| \/\/\/\/\/ \/\/\/\/\/ | \/\/\/                |             | \/\/\/\/\/ \/\/\/\/\/ | *   |
| \|\|\|\|\| \|\|\|     |                       |             | \|                    |     |
|                       |                       |             |                       | *   |
|                       |                       |             |                       |     |
|                       |                       |             |                       |     |


Each marked point is denoted by a vertical bar (oblique for points located in the *shit* area). Each line represents 10 points. Once the 10 points are entered on the line, we move on to the next one.
This until one of the players completes his column.

The * indicates *shit*. The player whose points are on this line cannot change cards.
If a player is in *shit*, the others can't announce from *Chicago*

On the grid above, players who have between 21 and 30 points inclusive (here Rio) and between 41 and 50 points inclusive are in *shit*.

# Course of game

Each turn is played in two round.

## 1st round

The dealer deals 5 cards one by one to all players.
In this round, the players have the possibility of changing from 0 to 5 cards, the goal being to obtain the biggest combination (see table).

If a player only wants to change one card, the dealer turns over the top card of the pile and shows it to everyone. The player has the option of taking or refusing this card. If he refuses it, the refused card is put at the bottom of the pile. The dealer deals the next card to the player face down.

Once everyone has changed their cards, the players take turns announcing the resulting combination. If a previous player has already announced a combination stronger than his, the player simply says "Not better" so as not to reveal his game. If he has nothing, the player says "Nothing" or if a combination has already been announced, he can say "Not better"

The player who obtained the strongest combination (the combinations are compared, then for the same combination, the value of the cards and the strongest suit in the event of a tie) scores the number of points corresponding to his combination.

Thus a pair of Queens is stronger than a pair of 8. If players have an identical combination with the same value of cards, the one with the strongest suit wins the points. For example, if 2 players have one pair of 10s. Whoever has the 10 of spades wins the point.

For a straight, for example, we first look at the value of the highest card in the straight, then in the event of a tie, we look at the one with the strongest suit for this card.

Players can again change the desired number of cards. The goal is to make the last trick to win 5 points (or 10 if he announced a *Chicago*) and/or have the best combination in the next step.

## 2nd round:

The first player of the turn, the one just after the dealer, must place a card in front of him face up. This card is considered an *asset*. The other players are obliged to supply the suit if they can otherwise they discard a card of another suit which will inevitably be the loser.

The next player, if he has one of the same suit, must place it, otherwise he discards a card of another suit still in front of him. And so on. Once all players have placed a card in front of them, whoever has the highest rank card in the suit requested by the first player wins. different suited cards
to that requested are necessarily losers. It is the turn of the player who won the plaice to start playing. He places the card of his choice on the one he already has in front of him. We start again in the same way until the last card of the hand. The player who wins the last trick (ie the fifth) earns 5 points or 10 if he announced a *Chicago*.

The combinations of each of the players are again compared. Whoever has the highest wins the number corresponding to his combination. This in the same way as in round 1.

The cards played are returned at the end of the draw, the player who has just distributed the cards cuts for the next dealer and we start again in round 1.

The game ends when one of the players has finished filling his column. In the case of this grid presented above, when a player score exceed 70 points.

### The Chicago

At the start of the 2nd round, one or more players can announce *Chicago* if they are sure of winning the last trick. The announcement must be made before the first card is laid. If he succeeds in the contract, he wins a bonus of 5 additional points and will therefore obtain 10 points at the end of the second round. On the other hand, if he fails he will lose 5 points on his current score.

### The shit

As indicated above:
- When there is a player in the *shit* nobody can announce *Chicago*.
- Player(s) in *shit* cannot change cards.
