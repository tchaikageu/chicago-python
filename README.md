# chicago-python

Terminal implemention of the Chicago cards game in python

Check chicago-rules-fr.md or chicago-rules-en.md for playing rules

## Installation instructions

Requirements:
- Python ≥ 3.8
- click
- pyfiglet
- rich
```
~$ git clone https://git.envs.net/tchaikageu/chicago-python.git chicago
~$ cd chicago
~$ pip install --user .
```

Then you can run the ```chicago``` command to enjoy playing the game

You can set your name in the game using ```chicago --name "your name"```
If you don't provide your name when using the ```--name``` option, the game will prompt for it

This can also be done by setting environment variable as follow :

```
export CHICAGO_NAME="your name"
```
To remove environment variable use :
```
~$ unset CHICAGO_NAME
```

Setup a game with a 10 lines grid (score to win is >=101) and shits zone in 3rd and 7th positions and 3 players

```
~$ chicago --lines 10 --shit 3 --shit 7 --players 3
```

## Uninstalling the game

```
~$ pip uninstall chicago
```
