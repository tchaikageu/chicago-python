"""Test combinations"""
from random import shuffle

import pytest

from chicago.card import Card
from chicago.combination import Combination
from chicago.hand import Hand
from chicago.rank import Rank
from chicago.suit import Suit

nada = [
    Card(suit=Suit.CLUBS, rank=Rank.KING),
    Card(suit=Suit.DIAMONDS, rank=Rank.EIGHT),
    Card(suit=Suit.HEARTS, rank=Rank.NINE),
    Card(suit=Suit.SPADES, rank=Rank.TEN),
    Card(suit=Suit.HEARTS, rank=Rank.JACK),
]
shuffle(nada)

pair = [
    Card(suit=Suit.CLUBS, rank=Rank.SEVEN),
    Card(suit=Suit.HEARTS, rank=Rank.SEVEN),
    Card(suit=Suit.DIAMONDS, rank=Rank.EIGHT),
    Card(suit=Suit.SPADES, rank=Rank.TEN),
    Card(suit=Suit.HEARTS, rank=Rank.JACK),
]
shuffle(pair)

two_pair = [
    Card(suit=Suit.CLUBS, rank=Rank.SEVEN),
    Card(suit=Suit.HEARTS, rank=Rank.SEVEN),
    Card(suit=Suit.DIAMONDS, rank=Rank.JACK),
    Card(suit=Suit.HEARTS, rank=Rank.JACK),
    Card(suit=Suit.SPADES, rank=Rank.TEN),
]
shuffle(two_pair)

three = [
    Card(suit=Suit.CLUBS, rank=Rank.SEVEN),
    Card(suit=Suit.HEARTS, rank=Rank.SEVEN),
    Card(suit=Suit.SPADES, rank=Rank.SEVEN),
    Card(suit=Suit.DIAMONDS, rank=Rank.EIGHT),
    Card(suit=Suit.HEARTS, rank=Rank.JACK),
]
shuffle(three)

straight = [
    Card(suit=Suit.CLUBS, rank=Rank.SEVEN),
    Card(suit=Suit.DIAMONDS, rank=Rank.EIGHT),
    Card(suit=Suit.HEARTS, rank=Rank.NINE),
    Card(suit=Suit.SPADES, rank=Rank.TEN),
    Card(suit=Suit.HEARTS, rank=Rank.JACK),
]
shuffle(straight)

straight2 = [
    Card(suit=Suit.DIAMONDS, rank=Rank.EIGHT),
    Card(suit=Suit.HEARTS, rank=Rank.NINE),
    Card(suit=Suit.SPADES, rank=Rank.TEN),
    Card(suit=Suit.HEARTS, rank=Rank.JACK),
    Card(suit=Suit.CLUBS, rank=Rank.QUEEN),
]
shuffle(straight2)

straight3 = [
    Card(suit=Suit.HEARTS, rank=Rank.NINE),
    Card(suit=Suit.SPADES, rank=Rank.TEN),
    Card(suit=Suit.HEARTS, rank=Rank.JACK),
    Card(suit=Suit.CLUBS, rank=Rank.QUEEN),
    Card(suit=Suit.DIAMONDS, rank=Rank.KING),
]
shuffle(straight3)

straight4 = [
    Card(suit=Suit.SPADES, rank=Rank.TEN),
    Card(suit=Suit.HEARTS, rank=Rank.JACK),
    Card(suit=Suit.CLUBS, rank=Rank.QUEEN),
    Card(suit=Suit.DIAMONDS, rank=Rank.KING),
    Card(suit=Suit.HEARTS, rank=Rank.ACE),
]
shuffle(straight4)

full = [
    Card(suit=Suit.CLUBS, rank=Rank.SEVEN),
    Card(suit=Suit.HEARTS, rank=Rank.SEVEN),
    Card(suit=Suit.SPADES, rank=Rank.SEVEN),
    Card(suit=Suit.DIAMONDS, rank=Rank.JACK),
    Card(suit=Suit.HEARTS, rank=Rank.JACK),
]
shuffle(full)

flush = [
    Card(suit=Suit.CLUBS, rank=Rank.SEVEN),
    Card(suit=Suit.CLUBS, rank=Rank.NINE),
    Card(suit=Suit.CLUBS, rank=Rank.TEN),
    Card(suit=Suit.CLUBS, rank=Rank.JACK),
    Card(suit=Suit.CLUBS, rank=Rank.QUEEN),
]
shuffle(flush)

four = [
    Card(suit=Suit.CLUBS, rank=Rank.NINE),
    Card(suit=Suit.DIAMONDS, rank=Rank.NINE),
    Card(suit=Suit.SPADES, rank=Rank.NINE),
    Card(suit=Suit.HEARTS, rank=Rank.NINE),
    Card(suit=Suit.SPADES, rank=Rank.ACE),
]
shuffle(four)

four_jack = [
    Card(suit=Suit.CLUBS, rank=Rank.JACK),
    Card(suit=Suit.DIAMONDS, rank=Rank.JACK),
    Card(suit=Suit.HEARTS, rank=Rank.JACK),
    Card(suit=Suit.SPADES, rank=Rank.JACK),
    Card(suit=Suit.HEARTS, rank=Rank.NINE),
]
shuffle(four_jack)

straight_flush = [
    Card(suit=Suit.CLUBS, rank=Rank.SEVEN),
    Card(suit=Suit.CLUBS, rank=Rank.EIGHT),
    Card(suit=Suit.CLUBS, rank=Rank.NINE),
    Card(suit=Suit.CLUBS, rank=Rank.TEN),
    Card(suit=Suit.CLUBS, rank=Rank.JACK),
]
shuffle(straight_flush)


@pytest.mark.parametrize(
    "cards, combination",
    [
        (nada, Combination.NADA),
        (pair, Combination.PAIR),
        (two_pair, Combination.TWO_PAIR),
        (three, Combination.THREE),
        (straight, Combination.STRAIGHT),
        (straight2, Combination.STRAIGHT),
        (straight3, Combination.STRAIGHT),
        (straight4, Combination.STRAIGHT),
        (full, Combination.FULL),
        (flush, Combination.FLUSH),
        (four, Combination.FOUR),
        (straight_flush, Combination.STRAIGHT_FLUSH),
        (four_jack, Combination.FOUR_JACK),
    ],
)
def test_combinations(cards, combination):
    """Test hands combination"""
    hand = Hand(cards)
    assert hand.combination == combination
