"""Test the Game object"""
import pytest

from chicago.chicago import ChicagoGame
from chicago.grid import Grid
from chicago.player_factory import playerfactory

grid = Grid()

players_settings = {
    "Tokyo": "computer",
    "El Professor": "computer",
    "Rio": "human_term",
    "Nairobi": "computer",
}

players = []
for name, player_type in players_settings.items():
    players.append(playerfactory(name=name, player_type=player_type))


def test_deck():
    """Test the deck len"""
    new_game = ChicagoGame(players, grid)
    assert len(new_game.deck) == 32


def test_players():
    """Test check player instance"""
    with pytest.raises(Exception):
        assert ChicagoGame([players[0], "Bidule"], grid)
