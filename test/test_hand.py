"""TestHand class"""

import pytest

from chicago.card import Card
from chicago.hand import Hand
from chicago.rank import Rank
from chicago.suit import Suit


def test_instance():
    """Instance test"""
    with pytest.raises(TypeError):
        assert Hand([1])


def test_dupe():
    """Test duplicate card error"""
    hand = Hand()
    card = Card(suit=Suit.HEARTS, rank=Rank.NINE)
    hand.append(card)
    with pytest.raises(Exception):
        assert hand.append(card)


def test_max_len():
    """Test max length error"""
    hand = Hand()
    cards = []
    for suit in [Suit.CLUBS, Suit.SPADES]:
        for rank in [Rank.TEN, Rank.EIGHT, Rank.NINE]:
            card = Card(suit=suit, rank=rank)
            cards.append(card)

    with pytest.raises(Exception):
        for card in cards:
            hand.append(card)

    with pytest.raises(Exception):
        hand = Hand(cards)
