"""Test the Grid object"""
import pytest

from chicago.grid import Grid

#
grid1 = Grid(max_score=100, shits=[2, 5, 7])
shitty_scores1 = [21, 25, 30, 51, 55, 71, 75, 80]
not_shitty_scores1 = [-10, -5, 0, 5, 10, 20, 31, 50, 61, 70, 81]
test1 = (grid1, not_shitty_scores1, shitty_scores1)

#
grid2 = Grid(max_score=90, shits=[1, 4, 8])
shitty_scores2 = [11, 15, 20, 41, 45, 50, 81, 85, 90]
not_shitty_scores2 = [-10, -2, 0, 5, 10, 21, 31, 40, 51, 61, 70, 80, 91]
test2 = (grid2, not_shitty_scores2, shitty_scores2)

grid3 = Grid()  # Default grid
shitty_scores3 = [21, 25, 30, 51, 55, 60]
not_shitty_scores3 = [-10, -2, 0, 5, 10, 20, 31, 40]
test3 = (grid3, not_shitty_scores3, shitty_scores3)


@pytest.mark.parametrize(
    "grid, not_shitty_scores, shitty_scores", [test1, test2, test3]
)
def test_grid(grid, not_shitty_scores, shitty_scores):
    """grid tests for shitty or none shitty scores"""
    assert not grid.are_shitty(not_shitty_scores)
    for score in not_shitty_scores:
        assert not grid.is_shitty(score)

    assert grid.are_shitty(shitty_scores)
    for score in shitty_scores:
        assert grid.is_shitty(score)
