""" player factory"""
from chicago.player_computer import PlayerComputer
from chicago.player_human_term import PlayerHumanTerm

types = {"human_term": PlayerHumanTerm, "computer": PlayerComputer}


def playerfactory(player_type: str, name: str):
    """Create player"""
    if player_class := types.get(player_type):
        return player_class(name)

    raise Exception(f"{player_type} is not implemented")
