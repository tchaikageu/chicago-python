"""Automatic change or keep card for non human player"""

import random
import time
from collections import Counter
from typing import List

from chicago.card import Card
from chicago.combination import Combination
from chicago.hand import Hand
from chicago.player import Player
from chicago.rank import Rank
from chicago.split import split_hand


def mysleep():
    """Custor sleep function"""
    time.sleep(1.2)


class PlayerComputer(Player):
    """Player class for computer player"""

    def __init__(self, name, cards=None):
        self.old_combination: Combination = Combination.NADA
        self.old_level: int = 0
        super().__init__(name, cards)

    def change_cards(self) -> List[Card]:
        """Return list of cards to change"""
        self.old_combination = self.hand.combination
        self.old_level = self.hand.level

        hand_cards, cards = split_hand(self.hand)
        self.hand = Hand(hand_cards)
        mysleep()
        if cards:
            print(f"{self} : {len(cards)} card(s) please...")
        else:
            print(f"{self} : No cards ! Thanks")
        return cards

    def keep_card(self, card: Card) -> bool:
        """Keep or skip first card shown by dealer"""
        new_hand = Hand(self.hand)
        new_hand.append(card)
        mysleep()
        if (
            new_hand.level > self.old_level and new_hand.combination > Combination.PAIR
        ) or (card.rank in [Rank.KING, Rank.ACE] and random.randint(0, 10) > 4):
            return True

        return False

    def play_card(self, played_cards: List[Card]) -> Card:
        """Auto play card"""

        # Default play random card. Need improvement
        card_to_play = random.choice(self.kept_cards)

        if played_cards:
            first_card = played_cards[0]
            playable_cards = self.playable_cards(first_card=first_card).copy()
            same_suit_ranks = [
                int(card.rank)
                for card in played_cards
                if int(card.suit) == int(first_card.suit)
            ]
            max_rank = max(same_suit_ranks)
            if int(playable_cards[0].suit) == int(first_card.suit):
                # Same suit playable cards
                playable_cards.sort(key=lambda card: int(card.rank), reverse=True)
                if int(playable_cards[0].rank) < max_rank:
                    # Play less ranked card if player can't win
                    playable_cards.sort(key=lambda card: int(card.rank))
            else:
                # play less ranked card for other suit
                playable_cards.sort(key=lambda card: int(card.rank))

            card_to_play = playable_cards[0]

        self.kept_cards = [card for card in self.kept_cards if card != card_to_play]
        mysleep()
        return card_to_play

    def do_chicago(self) -> bool:
        ranks_count = Counter([card.rank for card in self.hand])
        hand_level = 0
        min_for_true = 11  # Function return False by default

        for rank, count in ranks_count.items():
            hand_level += rank.coef * count

        if hand_level >= Rank.ACE.coef * 2 + Rank.KING.coef * 3:
            min_for_true = 6

        if hand_level >= Rank.ACE.coef * 3 + Rank.KING.coef:
            min_for_true = 4

            if self.hand.combination == Combination.FULL:
                min_for_true -= 3

        if hand_level >= Rank.ACE.coef * 4:
            min_for_true = 0

        if self.hand.combination == Combination.STRAIGHT_FLUSH:
            min_for_true -= 2

        return random.randint(0, 10) >= min_for_true
