"""Suit class definition"""
from enum import Enum


class Suit(Enum):
    """Suit class"""

    SPADES = "♠"
    HEARTS = "♡"
    DIAMONDS = "♢"
    CLUBS = "♣"

    def __str__(self) -> str:
        return self.value

    def __int__(self) -> int:
        """Return int suit value to compute comb_level"""
        if self == Suit.SPADES:
            return 3
        if self == Suit.HEARTS:
            return 2
        if self == Suit.DIAMONDS:
            return 1
        return 0

    def to_str(self) -> str:
        """Return suit name"""
        if self == Suit.SPADES:
            return "SPADES"
        if self == Suit.HEARTS:
            return "HEARTS"
        if self == Suit.DIAMONDS:
            return "DIAMONDS"
        return "CLUBS"
