"""Rank class definition"""
from enum import Enum


class Rank(Enum):
    """Rank Class"""

    SEVEN = 7
    EIGHT = 8
    NINE = 9
    TEN = 10
    JACK = 11
    QUEEN = 12
    KING = 13
    ACE = 14

    def __str__(self):
        if self is Rank.JACK:
            return "J"
        if self is Rank.QUEEN:
            return "Q"
        if self is Rank.KING:
            return "K"
        if self is Rank.ACE:
            return "A"
        return str(self.value)

    def __gt__(self, other: "Rank") -> bool:
        return self.value > other.value

    def __lt__(self, other: "Rank") -> bool:
        return self.value < other.value

    def __int__(self) -> int:
        """Return rank value"""
        return int(self.value - 7)

    @property
    def weighting(self) -> float:
        """Return weighting to keep or skip card"""
        return rank_weighting.get(self, 0.0)

    @property
    def coef(self) -> int:
        """Define coefficient for computer to claim Chicago or not"""
        return rank_coef.get(self, 0)


rank_weighting = {
    Rank.SEVEN: 0.0,
    Rank.EIGHT: 0.1,
    Rank.NINE: 0.2,
    Rank.TEN: 0.4,
    Rank.JACK: 0.5,
    Rank.QUEEN: 0.7,
    Rank.KING: 0.8,
    Rank.ACE: 1.0,
}

rank_coef = {
    Rank.SEVEN: 0,
    Rank.EIGHT: 0,
    Rank.NINE: 0,
    Rank.TEN: 1,
    Rank.JACK: 8,
    Rank.QUEEN: 64,
    Rank.KING: 512,
    Rank.ACE: 4096,
}

ALL_RANKS = " ".join(map(str, list(Rank)))
ALL_RANKS_LIST = ALL_RANKS.split(" ")
