"""Combination class"""
from enum import Enum


class Combination(Enum):
    """ "Combination class"""

    NADA = 0
    PAIR = 1
    TWO_PAIR = 2
    THREE = 3
    STRAIGHT = 4
    FULL = 5
    FLUSH = 6
    FOUR = 7
    STRAIGHT_FLUSH = 8
    FOUR_JACK = 9

    def __str__(self):
        """Print combination name"""
        return combination_to_string.get(self)

    def __int__(self) -> int:
        """Return int value for combination"""
        return self.value - 1

    def __gt__(self, other: "Combination") -> bool:
        return self.value > other.value

    @property
    def coef(self):
        """Coef to calculate combination level"""
        if self in (
            Combination.FULL,
            Combination.FLUSH,
            Combination.TWO_PAIR,
            Combination.STRAIGHT_FLUSH,
        ):
            return 1
        return 0


combination_to_string = {
    Combination.NADA: "NOTHING",
    Combination.PAIR: "ONE PAIR",
    Combination.TWO_PAIR: "TWO PAIR",
    Combination.THREE: "THREE OF A KIND",
    Combination.STRAIGHT: "STRAIGHT",
    Combination.FULL: "FULL HOUSE",
    Combination.FLUSH: "FLUSH",
    Combination.FOUR: "FOUR OF A KIND",
    Combination.STRAIGHT_FLUSH: "STRAIGHT FLUSH",
    Combination.FOUR_JACK: "FOUR JACK",
}
