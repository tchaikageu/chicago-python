"""Main app"""
import random

import click

from chicago.chicago import ChicagoGame
from chicago.grid import Grid
from chicago.player_factory import playerfactory


@click.command()
@click.option(
    "--name",
    default="Rio",
    prompt="What's your name",
    prompt_required=False,
    envvar="CHICAGO_NAME",
    show_default=True,
    help="Set your name in the game",
)
@click.option(
    "--lines",
    default=7,
    show_default=True,
    type=click.IntRange(3, 10),
    help="Set grid number of lines",
)
@click.option(
    "--players",
    "players_number",
    default=4,
    type=click.IntRange(2, 4),
    help="Set number of players",
)
@click.option(
    "--shit",
    multiple=True,
    default=[2, 5],
    show_default=True,
    help="Set shit(s) position (first line is 0)",
)
def run(name, lines, players_number, shit):
    """Run the game"""
    players_settings = {
        "Tokyo": "computer",
        "El Professor": "computer",
        "Rio": "human_term",
        "Nairobi": "computer",
    }

    players_number -= 1  # Keep a place for human player

    computers_name = [
        name for name in players_settings if players_settings.get(name) != "human_term"
    ]
    computers_name = random.sample(computers_name, players_number)
    computers_settings = {name: players_settings.get(name) for name in computers_name}
    human_setting = {
        name: players_settings.get(name)
        for name in players_settings
        if players_settings.get(name) == "human_term"
    }

    players_settings = {**computers_settings, **human_setting}

    grid = Grid(max_score=lines * 10, shits=list(shit))
    players = []

    for player_name, player_type in players_settings.items():
        if player_type == "human_term":
            player_name = name
        players.append(playerfactory(name=player_name, player_type=player_type))

    random.shuffle(players)
    new_game = ChicagoGame(players, grid)
    new_game.play()
