"""banner.py"""

from pyfiglet import Figlet
from rich import print as rprint


def banner_primary(title: str = None) -> None:
    """Primary banner"""
    if title:
        text = Figlet(font="cosmic").renderText(title)
        rprint(f"[red]{text}[/]")


def banner_secondary(title: str = None) -> None:
    """Secondary banner"""
    if title:
        text = Figlet(font="mini").renderText(title)
        print(text)


def banner_round(title: str = None) -> None:
    """Secondary banner"""
    if title:
        text = Figlet().renderText(title)
        rprint(f"[blue]{text}[/]")
