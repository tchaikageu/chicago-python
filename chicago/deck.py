"""Deck class"""
import random
from collections import deque

from chicago.card import Card
from chicago.rank import Rank
from chicago.suit import Suit


class Deck(deque):
    """Deck class"""

    def __init__(self):
        super().__init__([Card(suit=suit, rank=rank) for suit in Suit for rank in Rank])

    def shuffle(self):
        """Shuffle deck"""
        random.shuffle(self)

    def cut(self):
        """Cut deck"""
        cutter = random.randint(1, len(self) - 1)
        for _ in range(cutter):
            self.appendleft(self.pop())
