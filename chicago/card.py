"""
Card Class definition
"""
from typing import NamedTuple

from chicago.rank import ALL_RANKS_LIST, Rank
from chicago.suit import Suit


class Card(NamedTuple):
    """Card class"""

    suit: Suit
    rank: Rank

    def __str__(self) -> str:
        return f"{self.rank}{self.suit}"

    def __gt__(self, other) -> bool:
        return self.suit == other.suit and int(self.rank) > int(other.rank)

    @property
    def card_index(self) -> int:
        """Return card index"""
        return ALL_RANKS_LIST.index(str(self.rank))
