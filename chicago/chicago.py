"""Chicago game class"""
# import random
import time
from itertools import cycle
from typing import List

from click import getchar
from rich import print as rprint
from rich.panel import Panel
from rich.table import Table

from chicago.__init__ import __version__ as VERSION
from chicago.banner import banner_primary, banner_round, banner_secondary
from chicago.card import Card
from chicago.combination import Combination
from chicago.deck import Deck
from chicago.grid import Grid
from chicago.hand import MAX_HAND_LEN


class ChicagoGame:
    """Chicago Game class"""

    def __init__(self, players_list: list, game_grid: Grid):
        self.players = players_list
        self.players_copy = self.players
        if len(self.players) not in range(2, 5):
            raise Exception("The game is played by 2, 3 or 4 players")
        self.grid = game_grid
        self.check_players()
        self.deck = Deck()
        self.deck.shuffle()

    def check_players(self):
        """Check players"""
        human_players = 0
        for checked_player in self.players:
            if checked_player.is_human:
                human_players += 1

        if human_players > 1:
            raise Exception(
                "Only one human players is allowed not" f"{human_players} players"
            )

    @property
    def players_scores(self):
        """Players score lis"""
        return [player.score for player in self.players]

    def play(self):
        """Run the game"""
        while True:
            print("\033c")
            banner_primary("Chicago")
            rprint(Panel.fit(f"[yellow]v. {VERSION}[/]", border_style="red"))
            banner_secondary("THE CARDS GAME !")
            self.show_settings()
            game_steps = [
                self.deck.cut,
                self.serve_cards,
                self.first_round,
                self.second_round,
                self.show_scores_grid,
                self.players_rotate,
            ]

            game_cycle = cycle(game_steps)

            while not self.grid.has_winner(self.players_scores):
                next(game_cycle)()

            # Game ending
            self.show_scores_grid()
            winner_idx = self.grid.get_winner_idx(self.players_scores)
            winner = self.players[winner_idx]
            banner_round(f"{winner} wins")
            print(f"** The winner is {winner} whith {winner.score} points")
            for player in self.players:
                player.score = 0
                for _ in range(MAX_HAND_LEN):
                    if player.hand:
                        self.deck.appendleft(player.hand.pop())

            print("Play again ? (Y/N):")
            answer = getchar(False)
            if answer in ("n", "N"):
                banner_round("Bye !")
                print("\nHave a nice day.\n")
                break

    def show_settings(self):
        """Show game settings"""
        settings_txt = "Players:\n"
        for player in self.players:
            player_type = "[blue]Human[/]" if player.is_human else "Computer"
            settings_txt += f"- {player} : {player_type}\n"
        settings_txt += f"\nScore to win: [blue]{self.grid.max_score+1}"
        rprint(Panel.fit(settings_txt, border_style="green"))

    def serve_cards(self):
        """Serve cards to player"""
        for _ in range(MAX_HAND_LEN):
            for player in self.players:
                player.add_card(self.deck.pop())

    def first_round(self):
        """First round of the game"""
        banner_round("1st round")
        rprint(f"[italic green]The dealer is {self.players[-1]}[/]")
        self.show_hands_comb()
        self.players_change_cards()
        self.show_hands_comb(1)
        self.get_comb_winner()
        self.show_scores_grid()
        self.players_change_cards()
        self.show_hands_comb()

    def second_round(self):
        """2nd round of the game"""
        banner_round("2nd round")
        initial_players_list = self.players.copy()
        if self.grid.are_shitty(self.players_scores):
            rprint("[bold red]No Chicago possible ![/]")
        else:
            for player in self.players:
                player.chicago = player.do_chicago()
                if player.chicago:
                    rprint(f"[bold green]{player} claims Chicago[/]")
                else:
                    rprint(f"[blue]{player}: [italic]No Chicago[/]")

        print()

        for _ in range(MAX_HAND_LEN):
            played_cards = []
            winner: int = 0
            for player in self.players:
                player_card = player.play_card(played_cards)
                print(f"{player}: {player_card}")
                played_cards.append(player_card)
            winner = self.get_turn_winner(played_cards)
            time.sleep(1)
            rprint(
                f"[blue]Turn winner is {self.players[winner]}"
                f" with {played_cards[winner]}[/]\n"
            )
            # The winner is the first to play next turn
            self.players = self.players[winner:] + self.players[:winner]
        # Add 5 points to winner score
        self.players[0].score += 5
        rprint(
            f"[bold green]{self.players[0]} gets 5 points for winning last turn[/]\n"
        )
        self.chicago_check()
        time.sleep(1)
        self.show_hands_comb(2)
        self.get_comb_winner()
        # Getting players cards back to deck
        for player in self.players:
            for _ in range(MAX_HAND_LEN):
                self.deck.appendleft(player.hand.pop())
        self.players = initial_players_list.copy()

    def get_turn_winner(self, played_cards: List[Card]) -> int:
        """Return turn winner index"""
        best_card = played_cards[0]
        for card in played_cards[1:]:
            best_card = card if card > best_card else best_card

        return played_cards.index(best_card)

    def players_change_cards(self):
        """Player changing cards"""
        for player in self.players:
            cards_list = ""
            if self.grid.is_shitty(player.score):
                rprint(f"[red]{player} can't change (shit)[/]")
            else:
                cards_to_change = player.change_cards()
                if len(cards_to_change) == 1:
                    first_card = self.deck.pop()
                    self.deck.appendleft(cards_to_change[0])
                    rprint(
                        f"[italic green]{self.players[-1]}[/]: [default]{first_card}[/]"
                    )
                    if player.keep_card(first_card):
                        print(f"{player}: Oh yes please !")
                        player.add_card(first_card)
                    else:
                        print(f"{player}: No Thank you!")
                        self.deck.appendleft(first_card)
                        dealer_card = self.deck.pop()
                        player.add_card(dealer_card)
                        cards_list = f"{dealer_card}"

                else:
                    for card in cards_to_change:
                        dealer_card = self.deck.pop()
                        cards_list += f"{dealer_card} "
                        player.add_card(dealer_card)
                        self.deck.appendleft(card)
                if player.is_human and cards_list:
                    rprint("[italic]You get:[/] [default]" + cards_list)
                    rprint("[blue]Hit ANY KEY to resume...[/]")
                    getchar(False)
        print()

    def players_rotate(self):
        """Players rotation"""
        self.players = self.players[1:] + self.players[:1]

    def get_comb_winner(self):
        """Print winner name and combination"""
        winner = None
        best_comb_level = 0
        for player in self.players:
            if (comb_level := player.hand.level) > best_comb_level:
                best_comb_level = comb_level
                best_comb = player.hand.combination
                winner = player

        if best_comb_level == 0:
            rprint("[bold red]No one wins. All players are loosers![/]\n")
            return

        winner.score += int(best_comb) + 1
        rprint(
            f"[green]The combination winner is {winner} with {best_comb} "
            f"(combevel={best_comb_level})[/]"
            f"\n[green]Score +{int(best_comb)+1} for {winner}[/]\n"
        )
        if best_comb == Combination.FOUR_JACK:
            banner_round("4 Jacks !")
            rprint("[bold red]All other players are 0 scored now[/]\n")
            for player in self.players:
                if player != winner:
                    player.score = 0

    def show_scores_grid(self):
        """Display scores grid"""
        rprint(self.grid.scores_table(self.players_copy))
        rprint("[blue]Hit ANY KEY to resume...[/]\n")
        getchar(False)

    def show_hands_comb(self, show_level=0):
        """Show hand and combination for each player
        show_level=0: show only human hand, combination and level
        show_level=1: hide hands for computer
        show_level=2: show all players hands
        """
        table = Table(show_lines=True)
        columns = ["Player", "Hand", "Combination", "Level"]
        for text in columns:
            table.add_column(text)

        for player in self.players:
            if player.is_human or show_level == 2:

                table.add_row(
                    f"{player}",
                    f"{player.hand.sorted()}",
                    f"{player.hand.combination}",
                    f"{player.hand.level}",
                )
            elif show_level == 1:
                table.add_row(
                    f"{player}",
                    "[HIDDEN]",
                    f"{player.hand.combination}",
                    f"{player.hand.level}",
                )

        rprint(table)
        rprint("[blue]Hit ANY KEY to resume...[/]\n")
        getchar(False)

    def chicago_check(self) -> None:
        """Check Chicago claims"""
        for player in self.players:
            if player.chicago:
                if player != self.players[0]:
                    rprint(f"[bold red]{player} loose 5 points for failed Chicago[/]\n")
                    player.score -= 5
                else:
                    rprint(
                        f"[bold green]{player} gets 5 points more for the Chicago[/]\n"
                    )
                    player.score += 5
                player.chicago = False
