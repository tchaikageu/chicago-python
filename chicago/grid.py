"""Grid class"""
from typing import List

from rich.table import Table


class Grid:
    """Grid class"""

    def __init__(self, max_score: int = 70, shits: List[int] = None):
        if not shits:
            self.shits = [2, 5]
        else:
            self.shits = shits
        self.max_score: int = min(max_score, 100)
        self.max_score = max(30, self.max_score)
        self.check_shit()

    def is_shitty(self, score) -> bool:
        """Return True if score in shit zone"""
        for shit in self.shits:
            if score in range(shit * 10 + 1, shit * 10 + 11):
                return True
        return False

    def are_shitty(self, scores) -> bool:
        """Check if some scores are shitty"""
        for score in scores:
            if self.is_shitty(score):
                return True
        return False

    def check_shit(self):
        """Check shit zones values"""
        shits: List[int] = []
        for shit in self.shits:
            if shit * 10 + 1 < self.max_score and shit >= 0:
                shits.append(shit)
        self.shits = shits[:]

    def has_winner(self, scores_list: List[int]) -> bool:
        """Return True is there is a winner else False"""
        for score in scores_list:
            if score > self.max_score:
                return True
        return False

    def get_winner_idx(self, scores_list: List[int]) -> int:
        """Return winner index or -1 if there is no winner"""
        if scores_list and self.has_winner(scores_list):
            best_score_idx: int = 0
            for idx, score in enumerate(scores_list):
                if score > scores_list[best_score_idx]:
                    best_score_idx = idx
            return best_score_idx
        return -1

    def get_table_row(self, line, scores_list: List[int]):
        """Create row for rich table"""
        row = []
        for score in scores_list:
            min_score = line * 10 + 1
            if score >= min_score:
                mark = "[blue]|"
                if self.is_shitty(min_score):
                    mark = "[red]/"
                elif min_score > self.max_score:
                    mark = "[green]|"
                points = f"{mark}"
                for point in range(min_score, min_score + 9):
                    if score > point:
                        points += f"{mark}"
                        if (point + 1) % 5 == 0:
                            points += " "
                row.append(points)
            else:
                row.append(" ")
        return row

    def scores_table(self, players):
        """Return rich table whith dashed scores"""
        table = Table(show_lines=True)

        for player in players:
            table.add_column(f"{player} ({player.score})")
        table.add_column("s")

        rows = self.max_score // 10
        scores_list = [player.score for player in players]

        for line in range(rows + 1):
            row = self.get_table_row(line, scores_list)

            if self.is_shitty(line * 10 + 1):
                row.append("[red]*")
            elif line * 10 + 1 > self.max_score:
                row.append("[green]W")
            table.add_row(*row)

        return table
