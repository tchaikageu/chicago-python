"""Player class"""
from abc import ABC, abstractmethod
from typing import List, Optional

from chicago.card import Card
from chicago.hand import Hand


class Player(ABC):
    """Player class"""

    def __init__(self, name: str, cards=None) -> None:
        if cards is None:
            self._hand = Hand()
        else:
            self._hand = Hand(cards)

        self.name: str = name
        self.kept_cards: List[Card] = []
        self.score: int = 0
        self.chicago: bool = False
        self.is_human = False

    def __str__(self):
        return str(self.name)

    @property
    def hand(self) -> Hand:
        """Player hand"""
        return self._hand

    @hand.setter
    def hand(self, cards: List[Card]):
        self._hand = Hand(cards)

    def playable_cards(self, first_card: Card = None) -> List[Card]:
        """Return list of playable cards"""
        cards_list = []
        if first_card:
            cards_list = [
                card for card in self.kept_cards if card.suit == first_card.suit
            ]
        return cards_list or self.kept_cards

    def add_card(self, card: Card) -> None:
        """Add card to player"""
        self.hand.append(card)
        if len(self.hand) == 5:
            # sort cards in hand by suit and rank
            self.hand.sort(key=lambda card: [int(card.suit), int(card.rank)])
            self.kept_cards = Hand(self.hand)

    @abstractmethod
    def change_cards(self) -> Optional[List[Card]]:
        """Change cards function"""

    @abstractmethod
    def keep_card(self, card: Card) -> bool:
        """Keep or skip first card shown by dealer"""

    @abstractmethod
    def play_card(self, played_cards: List[Card]) -> Card:
        """Play card for 2nd round"""

    @abstractmethod
    def do_chicago(self) -> bool:
        """Do or not chicago"""
