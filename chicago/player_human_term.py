"""Backend function for human interaction"""
from typing import List, Optional

from click import getchar
from rich import print as rprint

from chicago.card import Card
from chicago.hand import Hand
from chicago.player import Player


class PlayerHumanTerm(Player):
    """Player Class for terminal game"""

    def __init__(self, name: str):
        super().__init__(name)
        self.is_human = True

    def change_cards(self) -> Optional[List[Card]]:
        """Ask human for changing cards"""
        cards_choice = ""
        for idx, card in enumerate(self.hand):
            cards_choice += f"[green]{idx+1}[/]:[default]{card} "
        rprint("[blue]Hit card number to change 2nd hit remove it from change list[/]")
        rprint("or [blue]Q[/] to end selection")
        rprint(cards_choice)
        index_list: List[int] = []
        while (answer := getchar(False)) not in ["q", "Q"]:
            try:
                card_number = int(answer) - 1
                if card_number in index_list:
                    print(f"Deleting {self.hand[card_number]} " "from change list")
                    index_list = [idx for idx in index_list if idx != card_number]

                elif card_number in range(len(self.hand)):
                    print(
                        f"Adding {self.hand[card_number]} "
                        f"to change list | hit {card_number+1} "
                        "to remove it"
                    )
                    index_list.append(card_number)
            except ValueError:
                pass

        print("Quitting cards selection")

        cards_list = [self.hand[index] for index in index_list]

        self.hand = Hand([card for card in self.hand if card not in cards_list])
        return cards_list

    def keep_card(self, _):
        """Ask human to keep card"""
        print("Keep this card ? (Y/N):")
        while (answer := getchar(False)) not in ["n", "N"]:
            if answer in ("y", "Y"):
                return True

        return False

    def play_card(self, played_cards: List[Card]) -> Card:
        """Human play card"""
        first_card = played_cards[0] if played_cards else None
        playable_cards = self.playable_cards(first_card)
        if len(playable_cards) == 1:
            card_to_play = playable_cards[0]
            print(f"{self}, you have only one playable card: {card_to_play}")
            rprint("[blue]Hit ANY KEY to continue...[/]")
            getchar(False)
        else:
            idx = 0
            txt = f"{self} select card to play: "
            for card in playable_cards:
                idx += 1
                txt += f"[green]{idx}[/]:[default]{card} "
            rprint(txt)
            while answer := getchar(False):
                try:
                    choice = int(answer)
                    choice -= 1
                    if choice in range(len(playable_cards)):
                        card_to_play = playable_cards[choice]
                        break
                except ValueError:
                    pass

        self.kept_cards = [card for card in self.kept_cards if card != card_to_play]
        return card_to_play

    def do_chicago(self) -> bool:
        print(f"Do you want to claim Chicago {self} ? (Y/N)")
        while (answer := getchar(False)) not in ["N", "n"]:
            if answer in ("Y", "y"):
                return True

        return False
