"""Split function for card changing"""
import random
from collections import Counter
from functools import partial
from typing import List, Tuple

from chicago.card import Card
from chicago.combination import Combination
from chicago.hand import Hand
from chicago.rank import ALL_RANKS, Rank
from chicago.suit import Suit


def keep_all(card: Card) -> bool:
    """Keep all cards"""
    return card is not None


def keep_high_ranks(card: Card) -> bool:
    """Keep ranks higher than rank"""
    return random.random() < card.rank.weighting


def keep_suit(suit: Suit, card: Card) -> bool:
    """Keep suit"""
    return card.suit == suit


def keep_ranks(ranks: List[Rank], card: Card) -> bool:
    """ "Keep one rank"""
    return card.rank in ranks


def keep_cards(cards_to_keep: List[Card], card):
    """Keep only cards in list"""
    return card in cards_to_keep


def keep_function(hand: Hand):
    """return function to keep caids"""

    two_ranks = Counter([card.rank for card in hand]).most_common(5)
    first_rank = two_ranks[0][0]
    second_rank = two_ranks[1][0]
    suits = Counter([card.suit for card in hand]).most_common()

    if hand.combination in (
        Combination.FLUSH,
        Combination.STRAIGHT,
        Combination.FULL,
        Combination.STRAIGHT_FLUSH,
        Combination.FOUR,
        Combination.FOUR_JACK,
    ):
        return keep_all

    if suits[0][1] == 4:
        # Near flush
        return partial(keep_suit, suit=suits[0][0])

    if suits[0][1] == 3 and hand.combination < Combination.THREE:
        return random.choice(
            [
                partial(keep_suit, suit=suits[0][0]),
                partial(keep_ranks, ranks=[first_rank, second_rank]),
                keep_high_ranks,
            ]
        )

    if hand.combination in (Combination.TWO_PAIR, Combination.THREE, Combination.PAIR):
        ranks = (
            [first_rank, second_rank]
            if hand.combination == Combination.TWO_PAIR
            else [first_rank]
        )

        return partial(keep_ranks, ranks=ranks)

    if hand.combination == Combination.NADA or (
        hand.combination == Combination.PAIR and first_rank < Rank.TEN
    ):
        cards_to_keep = split_near_straight(hand)
        if cards_to_keep:
            return partial(keep_cards, cards_to_keep)

    return keep_high_ranks


def split_hand(hand: Hand) -> Tuple[List[Card], List[Card]]:
    """Return tuple of 2 lists:
    - cards to keep
    - cards to discard
    """
    # combination, _ = get_comb_and_level(hand)
    keep_card = keep_function(hand)

    keep = [card for card in hand if keep_card(card=card)]
    discard = [card for card in hand if card not in keep]
    discard.sort(key=lambda card: int(card.rank))

    if len(discard) == 2 and hand.combination == Combination.THREE:
        # THREE cards : keep the best rank.
        # Better chance to get FULL or FOUR OF A KIND
        keep.append(discard.pop())

    if (len(discard) == 3 and hand.combination == Combination.PAIR) or (
        len(discard) == 0 and hand.combination in (Combination.NADA, Combination.PAIR)
    ):
        keep.extend([card for card in discard if keep_high_ranks(card)])

    discard = [card for card in hand if card not in keep]
    return keep, discard


def get_straights_list(hand: List[Card]) -> list:
    """Return list of straights in a player hand"""
    cards_ranks = [card.rank for card in hand]
    index = 0
    # Get straights list in hand
    straights_list = []

    while index < len(hand):
        last_index = index + 1
        while last_index <= len(hand):
            if " ".join(map(str, cards_ranks[index:last_index])) in ALL_RANKS:
                last_index += 1
            else:
                break
        straights_list.append(hand[index : last_index - 1])
        index = last_index - 1
    return straights_list


def split_near_straight(hand: List[Card]) -> List[Card]:
    """Check if hand contains near straight cards
    Return list of cards to keep
    """
    hand.sort(key=lambda card: card.rank)
    cards_ranks = [card.rank for card in hand]
    without_first = " ".join(map(str, list(cards_ranks[1:])))
    without_last = " ".join(map(str, list(cards_ranks[:-1])))
    # Basic check without first or last card
    if without_last in ALL_RANKS:
        return hand[:-1]
    if without_first in ALL_RANKS:
        return hand[1:]

    # Get straights list in hand
    straights_list = get_straights_list(hand)

    if len(straights_list) == 2:
        # Last card in first straight
        last_card = straights_list[0][-1]
        # First card in 2nd straight
        first_card = straights_list[1][0]
        if first_card.card_index - last_card.card_index == 2:
            return hand[1:]

    if len(straights_list) == 3 and len(straights_list[1]) == 3:
        last_card = straights_list[1][-1]
        first_card = straights_list[2][0]
        if first_card.card_index - last_card.card_index == 2:
            return hand[1:]

    return []
