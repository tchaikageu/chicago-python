"""Combination and level calculation"""
from collections import Counter
from typing import List, Tuple

from chicago.card import Card
from chicago.combination import Combination
from chicago.rank import ALL_RANKS, Rank
from chicago.suit import Suit


def get_comb_and_level(hand: List[Card]) -> Tuple[Combination, int]:
    """return combination and combination level"""
    # Counter for cards suits in player hand
    suits = Counter([card.suit for card in hand])
    # Get the ranks counter
    hand_ranks = Counter([card.rank for card in hand]).most_common(5)
    list_cards_ranks = [card.rank for card in hand]
    list_cards_ranks.sort()
    # Default values
    suit = 0
    combination_value = Combination.NADA

    if hand_ranks[0][1] == 4:
        combination_value = four_cards(hand_ranks)
    elif hand_ranks[0][1] == 3:
        combination_value = three_cards(hand_ranks)
    elif hand_ranks[0][1] == 2:
        combination_value = two_cards(hand_ranks)
        # Only keep the 2 pairs cards ranks to compute comb_level
        hand_ranks = (
            Counter([card.rank for card in hand]).most_common(2)
            if combination_value == Combination.TWO_PAIR
            else hand_ranks
        )
    elif " ".join(map(str, list_cards_ranks)) in ALL_RANKS:
        combination_value = (
            Combination.STRAIGHT_FLUSH if len(suits) == 1 else Combination.STRAIGHT
        )
        hand_ranks.sort(key=lambda x: x[0], reverse=True)
    elif len(suits) == 1:
        combination_value = Combination.FLUSH

    if combination_value in (
        Combination.PAIR,
        Combination.TWO_PAIR,
        Combination.STRAIGHT,
        Combination.FLUSH,
        Combination.STRAIGHT_FLUSH,
    ):
        if combination_value in (Combination.TWO_PAIR, Combination.FLUSH):
            # Sort cards by values not by counter
            hand_ranks.sort(key=lambda x: int(x[0]), reverse=True)
        for card_suit in Suit:
            if Card(rank=hand_ranks[0][0], suit=card_suit) in hand:
                suit = max(suit, int(card_suit))

    comb_level = (
        256 * int(combination_value)
        + 32 * int(hand_ranks[0][0])
        + 4 * int(hand_ranks[1][0]) * (combination_value.coef)
        + suit
    )
    comb_level = max(comb_level, 0)
    return combination_value, comb_level


def four_cards(ranks: List[Tuple[Rank, int]]) -> Combination:
    """Check combinations for 4 identicals cards ranks"""
    if ranks[0][0] == Rank.JACK:
        return Combination.FOUR_JACK
    return Combination.FOUR


def three_cards(ranks: List[Tuple[Rank, int]]) -> Combination:
    """Check combination for 3 identicals cards ranks"""
    if ranks[1][1] == 2:
        return Combination.FULL
    return Combination.THREE


def two_cards(ranks: List[Tuple[Rank, int]]) -> Combination:
    """Check ONE or TWO PAIR"""
    if ranks[1][1] == 2:
        return Combination.TWO_PAIR
    return Combination.PAIR
