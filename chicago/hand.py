"""Hand class"""

from collections import Counter

from chicago.card import Card
from chicago.comb_and_level import get_comb_and_level
from chicago.combination import Combination

MAX_HAND_LEN = 5


class NotCardInstance(TypeError):
    """Not a Card instance exception"""


class TooMuchCards(Exception):
    """Too much card in hand exception"""

    def __init__(self):
        super().__init__(f"Too much cards in hand. Max is {MAX_HAND_LEN} cards")


class CardAlreadyInHand(Exception):
    """Card already in hand"""


class Hand(list):
    """Hand class"""

    def __init__(self, cards=None):
        if cards is None:
            cards = []

        self._comb = Combination.NADA
        self._level = 0

        if len(cards) > MAX_HAND_LEN:
            raise TooMuchCards()

        super().__init__([self._validate_card(card) for card in cards])

        self._update_attributes()

    def __str__(self):
        txt = ""
        for card in self:
            txt += f"{card} "
        return txt.strip()

    def append(self, card: Card):
        if len(self) == MAX_HAND_LEN:
            raise TooMuchCards()

        super().append(self._validate_card(card))
        self._update_attributes()

    def _update_attributes(self):
        """update comb and level values"""
        if len(self) == MAX_HAND_LEN:
            self._comb, self._level = get_comb_and_level(self)
            # sort cards in hand by suit and rank
            self.sort(key=lambda card: [int(card.suit), int(card.rank)])

    def _validate_card(self, card) -> Card:
        """Check if card is a Card instance
        and card is not already in hand"""
        if not isinstance(card, Card):
            raise NotCardInstance(
                f"{card} is a {type(card)} instance not a Card instance"
            )

        # No dupe allowed
        if card in self:
            raise CardAlreadyInHand(f"'{card}' is a already in hand")
        return card

    def sorted(self) -> str:
        """Return hand string sorted acccrding to combination"""
        sorted_hand = self[:]
        if self.combination in [Combination.STRAIGHT, Combination.STRAIGHT_FLUSH]:
            sorted_hand.sort(key=lambda card: [int(card.rank)])
        elif self.combination != Combination.NADA:
            ranks = Counter([card.rank for card in self]).most_common(5)
            sorted_hand = []
            for rank in ranks:
                for card in self:
                    if card.rank == rank[0]:
                        sorted_hand.append(card)

        txt = ""
        for card in sorted_hand:
            txt += f"{card} "
        return txt.strip()

    @property
    def combination(self) -> Combination:
        """Return hand combination"""
        return self._comb

    @property
    def level(self) -> int:
        """Hand level"""
        return self._level

    @property
    def ranks(self):
        """Ranks counter"""

    @property
    def suits(self):
        """Suits counter"""
